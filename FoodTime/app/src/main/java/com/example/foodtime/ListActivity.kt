package com.example.foodtime

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodtime.ui.ListUiState
import com.example.foodtime.ui.ListViewModel
import com.example.foodtime.ui.theme.FoodTimeTheme

class ListActivity : ComponentActivity() {
    private lateinit var foodList: ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        foodList = intent.extras?.getStringArrayList("foodList") as ArrayList<String>

        setContent {
            FoodTimeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    FoodListManager()
                }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    private fun FoodListManager(
        listViewModel: ListViewModel = viewModel(),
        modifier: Modifier = Modifier
    ) {
        val listUiState by listViewModel.uiState.collectAsState()
        listViewModel.updateFoodList(createNewList(foodList))
        var foodChoice = rememberSaveable { mutableStateOf("") }

        Column() {
            Row() {
                TextField(
                    value = foodChoice.value,
                    onValueChange = { foodChoice.value = it },
                    label = { Text(stringResource(R.string.enter_a_food)) },
                )
                Button(onClick = {
                    if (foodChoice.value != "") {
                        foodList.add(foodChoice.value)
                        listViewModel.updateFoodList(createNewList(foodList))
                    }
                    // else toast error
                }) {
                    Text(stringResource(R.string.add_item))
                }
            }
            Column(modifier = modifier.verticalScroll(rememberScrollState())
                .fillMaxWidth()) {
                FoodListDisplay(listUiState, listViewModel)
                Button(onClick = {
                    setResult(Activity.RESULT_OK, Intent().apply {putExtra("foodList", foodList)} )
                    finish()
                }) {
                    Text(stringResource(R.string.save_changes))
                }
            }
        }
    }

    @Composable
    private fun FoodListDisplay(listUiState: ListUiState,
                                listViewModel: ListViewModel,
                                modifier: Modifier = Modifier) {
        Column(modifier = modifier) {
            listUiState.foodList.forEach { food ->
                Button(onClick = {
                    if (foodList.contains(food)) foodList.remove(food)
                    listViewModel.updateFoodList(createNewList(foodList))
                }) {
                    Text(food)
                }
            }
        }
    }
    private fun createNewList(list: MutableList<String>): MutableList<String> {
        var tempList = mutableListOf<String>()
        for (item in list) {
            tempList.add(item)
        }
        //if (add) tempList.add(newValue) else tempList.remove(newValue)
        //if (!add) tempList.remove(newValue)
        return tempList
    }
}