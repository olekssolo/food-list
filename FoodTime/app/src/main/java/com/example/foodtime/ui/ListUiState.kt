package com.example.foodtime.ui

data class ListUiState(
    val currentFood: String = "",
    var foodList: MutableList<String> = mutableListOf("Pizza", "Burger", "Ice cream", "Sushi", "Salad")
)