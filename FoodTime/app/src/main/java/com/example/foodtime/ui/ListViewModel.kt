package com.example.foodtime.ui

import androidx.lifecycle.ViewModel
import com.example.foodtime.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class ListViewModel() : ViewModel() {
    // maybe pass in the list here? into the ListUiState() ?
    private val _uiState = MutableStateFlow(ListUiState())
    val uiState: StateFlow<ListUiState> = _uiState.asStateFlow()

    private lateinit var currentFood: String
    private var foodList = mutableListOf("Pizza", "Burger", "Ice cream", "Sushi", "Salad")

    private fun randomFood(): String {
        return foodList.random()
    }
    fun setCurrentFood(food: String) {
        currentFood = food
        _uiState.value = ListUiState(currentFood = food)
    }
    fun updateFoodList(newList: MutableList<String>) {
        foodList = newList
        _uiState.value = ListUiState(foodList = newList)
    }
    fun getImageResource(food: String): Int {
        var resource = when (food) {
            "Pizza" -> R.drawable.pizza
            "Пицца" -> R.drawable.pizza
            "Burger" -> R.drawable.burger
            "Бургер" -> R.drawable.burger
            "Ice cream" -> R.drawable.ice_cream
            "Crème glacée" -> R.drawable.ice_cream
            "Мороженое" -> R.drawable.ice_cream
            "Sushi" -> R.drawable.sushi
            "Суши" -> R.drawable.sushi
            "Salad" -> R.drawable.salad
            "Salade" -> R.drawable.salad
            "Салат" -> R.drawable.salad
            else -> {
                R.drawable.default_image
            }
        }
        return resource
    }

    init {
        setCurrentFood(randomFood())

    }
}