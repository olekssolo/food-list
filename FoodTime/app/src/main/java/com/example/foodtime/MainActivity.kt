package com.example.foodtime

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodtime.ui.ListViewModel
import com.example.foodtime.ui.theme.FoodTimeTheme
import kotlin.random.Random
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.ui.Alignment
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.toLowerCase
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class MainActivity : ComponentActivity() {
    private lateinit var foodList: ArrayList<String>

    private val foodListActivityLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult())
        {
            activityResult ->
            if (activityResult.resultCode == Activity.RESULT_OK) {
                foodList = activityResult.data?.extras?.getStringArrayList("foodList") as ArrayList<String>
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /**
         * I couldn't figure out how to get the correct language to show on the first food but
         * it should be fine after one re-roll. So, initially the food will be in English.
         */
        val foodListResource = resources.getStringArray(R.array.food_list)
        foodList = arrayListOf()
        for (food in foodListResource) {
            foodList.add(food)
        }

        setContent {
            FoodTimeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    SuggestionMenu()
                }
            }
        }
    }

    @Composable
    fun SuggestionMenu(
        listViewModel: ListViewModel = viewModel()
    ) {
        val context = LocalContext.current
        val listUiState by listViewModel.uiState.collectAsState()
        val configuration = LocalConfiguration.current
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Row(
                modifier = Modifier,
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column {
                    Text(stringResource(R.string.you_should_eat))
                    // food icon here
                    Text(
                        listUiState.currentFood,
                        fontSize = 30.sp
                    )
                }
                Image(
                    painter = painterResource(listViewModel.getImageResource(listUiState.currentFood)),
                    contentDescription = "Image of current food",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .width(500.dp)
                        .fillMaxHeight()
                        .padding(5.dp)
                        .clickable(
                            onClick = {
                                context.startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("geo:0,0?q=${listUiState.currentFood}")
                                    )
                                )
                            }
                        )
                )
                Column(
                    modifier = Modifier.padding(8.dp)
                ) {
                    Button(onClick = {
                        listViewModel.setCurrentFood(foodList.random())
                    }) {
                        Text(stringResource(R.string.reroll))
                    }
                    Button(onClick = {
                        context.startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse(
                                "https://www.google.com/search?q=${listUiState.currentFood}")
                            )
                        )
                    }) {
                        Text(stringResource(R.string.find_food))
                    }
                    Button(onClick = {
                        foodListActivityLauncher.launch(
                            Intent(context, ListActivity::class.java)
                                .putExtra(
                                    "foodList", foodList
                                )
                        )
                    }) {
                        Text(stringResource(R.string.change_list))
                    }
                }
            }
        } else {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(stringResource(R.string.you_should_eat))
                // food icon here
                Text(
                    listUiState.currentFood,
                    fontSize = 30.sp
                )
                Image(
                    painter = painterResource(listViewModel.getImageResource(listUiState.currentFood)),
                    contentDescription = "Image of current food",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(300.dp)
                        .padding(5.dp)
                        .clickable(
                            onClick = {
                                context.startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("geo:0,0?q=${listUiState.currentFood}")
                                    )
                                )
                            }
                        )
                )
                Row(
                    modifier = Modifier.padding(8.dp)
                ) {
                    Button(onClick = {
                        listViewModel.setCurrentFood(foodList.random())
                    }) {
                        Text(stringResource(R.string.reroll))
                    }
                    Button(onClick = {
                        context.startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse(
                                "https://www.google.com/search?q=${listUiState.currentFood}")
                            )
                        )
                    }) {
                        Text(stringResource(R.string.find_food))
                    }
                    Button(onClick = {
                        foodListActivityLauncher.launch(
                            Intent(context, ListActivity::class.java)
                                .putExtra(
                                    "foodList", foodList
                                )
                        )
                    }) {
                        Text(stringResource(R.string.change_list))
                    }
                }
            }
        }
    }
}